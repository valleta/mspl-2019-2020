---
title: "Evolution de la popularité du prénom Louis de 1910 à 2018"
author: "Aurelien Vallet"
date: "Fevrier 2020"
output:
  pdf_document: default
  html_document:
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Using the [given names data set of INSEE](https://www.insee.fr/fr/statistiques/fichier/2540004/dpt2018_txt.zip), answer some of the following questions:

- Choose a first name (yours) analyse the evolution of its frequency along time?
- What can we say about ``Your name here'' (for each state, all the country)?
- Is there some sort of geographical correlation with the data? (optional)
- Which department has a larger variety of names along time? (optional)
- _your own question_ (be creative, not optional)

You need to use the _tidyverse_ for this analysis. Unzip the file _dpt2018_txt.zip_ (to get the **dpt2018.txt**). Read in R with this code. Note that you might need to install the `readr` package with the appropriate command.

## Download Raw Data from the website
```{r}
file = "dpt2018_txt.zip"
if(!file.exists(file)){
  download.file("https://www.insee.fr/fr/statistiques/fichier/2540004/dpt2018_csv.zip",
	destfile=file)
}
unzip(file)

```

## Build the Dataframe from file

```{r}
library(tidyverse)
df <- read_delim("dpt2018.csv",delim=";");
head(df)
```


```{r}
library(ggplot2);

  a <-  df  %>% filter(preusuel == "JEAN" | preusuel == "LOUIS") %>% group_by(annais, preusuel) %>% summarize(total = sum(nombre), total_Louis = sum(nombre[preusuel == "LOUIS"])) 
b <-  a %>%    group_by(annais) %>% summarize(proportion_louis_jean = sum(total_Louis) / sum(total)*100) 

b %>% ggplot(aes(x = annais, y = proportion_louis_jean)) + geom_point()  + scale_x_discrete(breaks=seq(1900, 2020, 10)) + xlab("Temps (Année)") + ylab("Proportion de Louis / Louis+Jean (en %)") + ggtitle("Popularité du prénom Louis de 1900 à 2018") + theme(plot.title = element_text(hjust = 0.5))

```


Ce Graphique à pour but de montrer l'évolution de la popularité du prénom Louis sur la période s'étalent de 1900 à 2018, pour calculer la popularité nous calculons chaque annnée la proportion du prénom Loui sur le nombre de prénom Louis et de Jean, nous constatons les évolutions suivantes : 

La proportion du prénom "Louis" par rapport au nombre de prenom "Louis" et "Jean" a diminué de 1900 à 1945, le prénom
Louis était donc en perte de popularité.

La tendance c'est ensuite stabilisée entre de 1945 à 1980, le prénom est resté "impopulaire" sur cette periode sans grande variation.

De 1980 à 2005 la proportion de Louis a augmentée ce qui laisse penser que le prénom est devenue plus populaire ou que le prénom Jean a perdu en popularité, puis 
depuis 2005 cette proportion tend à se stabiliser.














